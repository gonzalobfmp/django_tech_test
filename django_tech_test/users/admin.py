# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.mail import send_mail

from .models import User, Application
from datetime import datetime

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
            ('User Profile', {'fields': ('name',)}),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username', 'name', 'is_superuser')
    search_fields = ['name']


def make_accepted(modeladmin, request, queryset):
    queryset.update(accepted=datetime.utcnow())
    for application in queryset:
        user = application.user
        send_mail(subject='Application accepted',
                  message='HI {name}, your application with id {id} has been accepted'.format(name=user.username,
                                                                                              id=application.id),
                  from_email='admin@example.com',
                  recipient_list=[user.email])

make_accepted.short_description = "Accept selected applications"


@admin.register(Application)
class ApplicationListAdmin(admin.ModelAdmin):
    list_display = ("user", "id", "amount", "days", "reason", )
    search_fields = ['id']
    actions = [make_accepted]

