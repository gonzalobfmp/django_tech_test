# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),
    url(
        regex=r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),
    url(
        regex=r'^(?P<username>[\w.@+-]+)/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^~update/$',
        view=views.UserUpdateView.as_view(),
        name='update'
    ),
    url(
        regex=r'^~profile/$',
        view=views.ProfileUpdateView.as_view(),
        name='update_profile'
    ),

    url(
        regex=r'^~application/$',
        view=views.NewApplicationView.as_view(),
        name='new_application'
    ),

    url(
        regex=r'^~application_list/$',
        view=views.ApplicationListView.as_view(),
        name='application_list'
    ),
]
