# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from datetime import datetime

from django.contrib.auth.models import AbstractUser, User
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15, blank=True)
    company_name = models.CharField(blank=True, max_length=255)
    address = models.CharField(blank=True, max_length=1000)
    registered_number = models.CharField(blank=True, max_length=8)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Application(models.Model):
    amount = models.IntegerField(null=False, validators=[MinValueValidator(10000), MaxValueValidator(100000)])
    days = models.IntegerField(null=False)
    reason = models.TextField(null=False)
    user = models.ForeignKey(User)
    created = models.DateTimeField(default=datetime.now, blank=True)
    accepted = models.DateTimeField(null=True)

    def is_accepted(self):
        return self.accepted is not None

    is_accepted = property(is_accepted)


