
from django import forms
from .models import Profile, Application


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ("phone", "company_name", "address", "registered_number",)

    def clean_registered_number(self):
        registered_number = self.cleaned_data["registered_number"]
        if not len(registered_number) == 8:
            raise forms.ValidationError("Registered number for the company must have 8 digits")


class ApplicationForm(forms.ModelForm):

    class Meta:
        model = Application
        fields = ("id", "amount", "days", "reason", "is_accepted")