# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, RedirectView, UpdateView

from .models import Application, Profile, User


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class ProfileUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['phone',    'company_name',    'address',    'registered_number',]

    # we already imported User in the view code above, remember?
    model = Profile

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the User record for the user making the request
        user = User.objects.get(username=self.request.user.username)
        return user.profile


class NewApplicationView(LoginRequiredMixin, CreateView):

    fields = ["amount", "days", "reason", ]

    # we already imported User in the view code above, remember?
    model = Application

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def form_valid(self, form):
        application = form.save(commit=False)
        application.user = User.objects.get(username=self.request.user.username)  # use your own profile here
        application.save()
        return HttpResponseRedirect(self.get_success_url())


class ApplicationDescription(LoginRequiredMixin, DetailView):
    fields =['id', 'amount', 'days', 'reason']


class ApplicationListView(LoginRequiredMixin, ListView):
    model = Application

    def get_queryset(self):
        return User.objects.get(username=self.request.user.username).application_set.all().order_by('-created')
